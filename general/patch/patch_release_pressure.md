# Patch release pressure

**Note: The information here will be apply once the [mainteinance
policy has been extended] to account for three releases**

The patch release pressure is defined as the number of unreleased
merge requests merged into the active stable branches according to
the [maintenance policy].

## Release manager dashboard

The patch release pressure can be found in the [release manager dashboard]
in two areas: "Summary" and "Patch release pressure".

### Summary

Contains an overview of:

* The number of S1/S2 merge requests merged into stable branches
  that haven't been released.
* The number of merge requests merged into stable branches regardless
  of severity.

![Summary](images/summary_panels.png)

### Patch release pressure

Contains a breakdown of:

* The number of S1/S2 merge requests merged into stable branches
  per version.
* The number of merge requests merged into stable branches per
  version regardless of severity.

![breakdown per version](images/patch_release_pressure_panels.png)

## When to perform a patch release?

Release Managers can use the information on the [release manager dashboard]
to determine if a patch release may be required by:

1. Taking a look at the "S1/S2" panel in the Overview section. If this metric
   lists pending merge requests, a patch release is strongly encouraged.
1. Taking a look at the "Total" panel in the Summary section. If there is a
   considerable number of merge requests pending (e.g. 10), consider performing
   a patch release to reduce the pressure. This is recommended but not required.
   [Other factors](process.md#when-to-perform-a-patch-release) may help direct this decision.

[mainteinance policy has been extended]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/828
[maintenance policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[release manager dashboard]: https://dashboards.gitlab.net/d/delivery-release_management/delivery-release-management?orgId=1&refresh=5m
