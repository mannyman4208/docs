# How to sync Security repository with Canonical repository?

During the 'early-merge' phase of a security release, security merge requests targeting the
default branch are merged and deployed to GitLab.com at the same pace as regular security
merge requests. This causes Canonical and Security repositories to diverge.

Until the security release is published, the merge-train is automatically activated to keep
these repositories in sync. This automated task is handled by the [`security:merge_train` pipeline schedule],
which in turn executes the [`security:merge_train` task in release-tools].

Outside of this automation, there are two ways to sync the Security and Canonical changes.

## Using the pipeline schedule in Ops.

1. Go to https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules
1. Click on `Play` on the `gitlab-org/gitlab@master -> gitlab-org/security/gitlab@master` pipeline schedule.

## Manually syncing the repositories

Use this option in case of a merge-train failure due to conflicts.


1. Clone the GitLab repository:

```ssh
git clone git@gitlab.com:gitlab-org/gitlab.git # This may take some time
```

2. Add the security remote:

```ssh
git add remote security git@gitlab.com:gitlab-org/security/gitlab.git
``` 

3. Fetch the content from both remotes:

```ssh
git fetch origin
git fetch security
```

4. Create a branch from `security/master`

```ssh
git checkout master --track security/master
git checkout  -b sync-security-with-canonical
```

5. Merge `origin/master` into your branch, at this point conflicts will need to be resolved

```ssh
git merge origin/master`
```

6. Push the changes into `security` (**be careful of not pushing the changes to Canonical**)


```ssh
git push security sync-security-with-canonical
```

7. Create a merge request
8. Wait until the pipeline finishes.
9. [Disable the "Squash commits" setting](#disable-the-squash-commits-setting). 
10. If the pipeline is red and the failure is unrelated to the merge request, [disable the Pipelines must succeed setting](#disable-the-pipelines-must-succeed-setting).
11. Assign the merge request to your release manager counterpart for approval and merging.
12. After merging, **restore the "Squash commits" and the "Pipelines must succeed" settings**. See below.

### Disable the "Squash commits" setting.

As stated on [gitfaq](https://github.com/git/git/blob/083e01275b81eb6105247ec522adf280d99f6b56/Documentation/gitfaq.txt#L248-L274),
squash merges can cause the "possibility of needing to re-resolve conflicts again and again".

To disable the "Squash commits" setting:
1. Go to [merge requests settings on GitLab security repository]
1. Under the `Squash commits when merging` section, select `Encourage - Checkbox is visible and selected by default.` and save changes.
1. Open the merge request edit page, disable the `Squash commits when merge request is accepted` option, and save changes.
1. Merge the merge request.
1. After merging, restore the "Squash commits" check on the [merge requests settings on GitLab security repository]:
   * Select  `Require - Squashing is always performed. Checkbox is visible and selected, and users cannot change it.` under `Squash commits when merging` and save changes.

### Disable the "Pipelines must succeed" setting. 

The merge request can fail due to the danger-review job failing, which [outputs a comment with error `Feature flags are discouraged from security merge requests.`]

1. Wait until the merged result pipeline is passing on everything except the `danger-review` job.
2. Go to the [merge requests settings on GitLab security repository] and under `Merge checks`, disable the "Pipelines must succeed" check box
3. Save changes
4. Refresh the page of the merge request
5. Merge the merge request
6. Write on the merge request why it was force-merged
7. Re-enable the "Pipelines must succeed" check box, and make sure to save your changes

These steps should unblock the merge train jobs.

[`security:merge_train` task in release-tools]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/security.rake#L31
[`security:merge_train` pipeline schedule]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/121/edit
[outputs a comment with error `Feature flags are discouraged from security merge requests.`]: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2942#note_1188542112
[merge requests settings on GitLab security repository]: https://gitlab.com/gitlab-org/security/gitlab/-/settings/merge_requests
